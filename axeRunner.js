const pa11y = require('pa11y');
const html = require('pa11y-reporter-html');
const report = require('./report');
const options = require('./options');

var url = 'https://www.pa11y.org/' // Your URL here

var actions = [
    'click element .clearfix > :nth-child(2) > .site-nav__link'
]

var filter = {
    actions: actions,
    options: options.axeOptions,
    screenCapture: 'report/result.png',
    log: {
        debug: console.log,
        error: console.error,
        info: console.info
    }
}

pa11y(url, filter).then(async results => {
    console.log(results)
    const htmlResults = await html.results(results);
    const reportName = report.reportName(results.pageUrl)
    report.save(reportName, htmlResults)
});


// example without actions

pa11y('https://gitlab.com/explore', options.axeOptions).then(async results => { 
    const htmlResults = await html.results(results);
    const reportName = report.reportName(results.pageUrl)
    report.save(reportName, htmlResults)
});
