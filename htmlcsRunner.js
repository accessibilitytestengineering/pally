const pa11y = require('pa11y');
const html = require('pa11y-reporter-html');
const report = require('./report');
const options = require('./options');

var url = 'https://www.pa11y.org/' // Your URL here

var actions = [
    'click element .clearfix > :nth-child(3) > .site-nav__link',
    'screen capture report/resultHtmlcs.png'
]

var filter = {
    actions: actions,
    options: options.htmlcsOptions,
    log: {
        debug: console.log,
        error: console.error,
        info: console.info
    }
}

pa11y(url, filter).then(async results => { 
    const htmlResults = await html.results(results);
    const reportName = report.reportName(results.pageUrl)
    report.save(reportName, htmlResults)
});

// example without actions

pa11y('https://gitlab.com/explore', options.htmlcsOptions).then(async results => { 
    const htmlResults = await html.results(results);
    const reportName = report.reportName(results.pageUrl)
    report.save(reportName, htmlResults)
});