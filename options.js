module.exports = {

    axeOptions : {
        
        runners: [
            'axe'
        ]
    },

    htmlcsOptions : {
        actions:[],
        runners: [
            'htmlcs'
        ]
    }
}