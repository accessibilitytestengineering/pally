var fs = require('fs');

const save = (reportName, htmlResults) => {
    var filename = "report/" +                    
                   reportName + '_' +
                   new Date().toISOString().substring(0,10) 
                   + ".html"
    fs.writeFile(filename, htmlResults, (err) => {
        if (err) {
            return console.log(err);
        }
        console.log("The report file was saved under the name "+ reportName);
    });
}

const reportName = (url) => {
    var reportName = url.replace(/^.*\/\/[^\/]+/, '').substring(1)
    if (reportName) {
        reportName = reportName.replace(/\//g, '_')
    } else {
        reportName = "report"
    }
    return reportName
}

module.exports = {
    save: save,
    reportName : reportName
}